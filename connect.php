<?php 
	
require_once __DIR__ . '/src/Facebook/autoload.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
$database = new DataBase(DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME);
// Init PHP Sessions
session_start();
$loginUrl;
$fb = new Facebook([
  'app_id' => '499146490266725',
  'app_secret' => '69490884bed63df24238f86922555670',
]);

$helper = $fb->getRedirectLoginHelper();

if (!isset($_SESSION['facebook_access_token'])) {
  $_SESSION['facebook_access_token'] = null;
}

if (!$_SESSION['facebook_access_token'] && isset($_GET['connect'])) {
  $helper = $fb->getRedirectLoginHelper();
  try {
    $_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
    if ($_SESSION['facebook_access_token'] != null) {
      $response = $fb->get('/me?fields=id,name', $_SESSION['facebook_access_token']);
      //print_r($response->getDecodedBody());
      $user_id = $response->getDecodedBody()['id'];
      $user = $database->getRow("SELECT public.users.* FROM public.services LEFT JOIN public.users ON public.services.isikukood = public.users.isikukood WHERE public.services.authkey = ?", array($user_id));
      //print_r($user);
    }
    if ($user) {
      $_SESSION["isikukood"] = $user['isikukood'];
		  $_SESSION["eesnimi"] = $user['eesnimi'];
		  $_SESSION["perenimi"] = $user['perenimi'];
    } else {
      if (isset($_SESSION["isikukood"]) && isset($_SESSION['facebook_access_token']) && strlen($_SESSION['facebook_access_token'])> 0) {

           //print_r($response);
           $result = $database->insertRow("INSERT into public.services VALUES (?, ?, ?)", array($_SESSION["isikukood"],$user_id, "fb"));
           //echo "connected fb user to id card";
      } else {
        unset($_SESSION['facebook_access_token']);
        //echo "<div><b>you should be ashamed</b></div>";
      }
    }
    
    
  } catch(FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
  } catch(FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
  }
}

if (isset($_SESSION['facebook_access_token']) && $_SESSION['facebook_access_token'] != null) {
   $response = $fb->get('/me?fields=id,name', $_SESSION['facebook_access_token']);
   $user_id = $response->getDecodedBody()['id'];
  $user = $database->getRow("SELECT public.users.* FROM public.services LEFT JOIN public.users ON public.services.isikukood = public.users.isikukood WHERE public.services.authkey = ?", array($user_id));
  if ($user) {
      //echo "logged in as existing fb user";
      $_SESSION["isikukood"] = $user['isikukood'];
		  $_SESSION["eesnimi"] = $user['eesnimi'];
		  $_SESSION["perenimi"] = $user['perenimi'];
    }
} else {
  $permissions = [];
  $loginUrl = $helper->getLoginUrl('http://54.93.107.75/vorgurakendused2/index.php?connect', $permissions);
} 


	//connect with fb
	//header("Location: index.php");