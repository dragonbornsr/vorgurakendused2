<?php

class Logger {
	private static $file;

	public static function init() {
		// TODO: Check logger folder existance
		$filename = dirname(__FILE__).'/log/log-' . date('Y-m-d') . '.txt';
		Logger::$file = fopen($filename, "a");
	}

	/*public static function __destruct() {
		fclose(Logger::$file);
	}*/

    public static function log($type, $msg) {
			$str = "[" . date("Y/m/d h:i:s", time()) . "] (".$type."): " . $msg . "\r\n";
			fwrite(Logger::$file, $str);
    }
}

Logger::Init();

