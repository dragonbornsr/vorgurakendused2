<?php 
	require_once('config/config_db.php');
	require_once('idcardauth.php');
	require_once("dbConnectionPG.inc.php");
	$database = new DataBase(DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME);
	
	$user = IDCardAuth::getUser();
	
	if ($user) {
		$result = $database->insertRow("INSERT INTO public.users values (?, ?, ?)", array($user[2], $user[1], $user[0]));
		session_start();
		$_SESSION["isikukood"] = $user[2];
		$_SESSION["eesnimi"] = $user[1];
		$_SESSION["perenimi"] = $user[0];
		
	} else {
		session_unset();
		session_destroy(); 
	}
	
	header("Location: http://54.93.107.75/vorgurakendused2/index.php");