<?php 

class IDCardAuth {
    static function getUser () {
      
        $ident=getenv("SSL_CLIENT_S_DN");  
        $verify=getenv("SSL_CLIENT_VERIFY");
        if (!$ident || $verify!="SUCCESS") return False;
        $ident=IDCardAuth::certstr2utf8($ident);
        $ps=strpos($ident,",SN=");
        $pg=strpos($ident,",GN=");
        $pc=strpos($ident,"serialNumber=");
        if (!$ps || !$pg) return False;
        $pse=strpos($ident,",",$ps+1);
        $pge=strpos($ident,",",$pg+1);
        $pce=strpos($ident,",",$pc+1); 
        $res=array(substr($ident,$ps+4,$pse-($ps+4)), 
                  substr($ident,$pg+4,$pge-($pg+4)), 
                  substr($ident,$pc+13,11));
        return $res;
    }  
  
    static function certstr2utf8 ($str) {
        $str = preg_replace ("/\\\\x([0-9ABCDEF]{1,2})/e", "chr(hexdec('\\1'))", $str);       
        $result="";
        $encoding=mb_detect_encoding($str,"ASCII, UCS2, UTF8");
        if ($encoding=="ASCII") {
            $result=mb_convert_encoding($str, "UTF-8", "ASCII");
        } else {
            if (substr_count($str,chr(0))>0) {
                $result=mb_convert_encoding($str, "UTF-8", "UCS2");
            } else {
                $result=$str;
            }
        }
        return $result;
    }
}


//  Actual script to run
/*
$user=get_user();
if (!$user) echo ("Authentication failed.");
else {
  echo "Last name: " . $user[0] . "<br>";
  echo "First name: " . $user[1] . "<br>";
  echo "Person code: " . $user[2] . "<br>";
}

?>*/