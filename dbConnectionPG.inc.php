<?php 
    
    require_once("logger.inc.php");
    require_once('config/config_db.php');

    class DataBase {
        public $isConnected;
        protected $datab;
        
        public function __construct($username, $password, $host, $dbname, $options=array()){  
            try { 
                $this->datab = new PDO("pgsql:host={$host};dbname={$dbname}", $username, $password, $options); 
                $this->datab->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
                $this->datab->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
                $this->isConnected = true;
            } 
            catch (PDOException $e) { 
                $this->isConnected = false;
                Logger::log("DATABASE", $e->getMessage());
                return false;
            }
        }
        
        public function disconnect(){
            $this->datab = null;
            $this->isConnected = false;
        }
        
        public function getRow($query, $params=array()){
            try{ 
                $stmt = $this->datab->prepare($query); 
                $stmt->execute($params);
                return $stmt->fetch();  
            } catch(PDOException $e) {
                    Logger::log("DATABASE", $e->getMessage());
                    return false;
            }
        }
        public function getRows($query, $params=array()){
            try{ 
                $stmt = $this->datab->prepare($query); 
                $stmt->execute($params);
                return $stmt->fetchAll();       
                }catch(PDOException $e){
                    Logger::log("DATABASE", $e->getMessage());
                    return false;
            }       
        }
        public function insertRow($query, $params){
            try{ 
                $stmt = $this->datab->prepare($query); 
                 return $stmt->execute($params);
                }catch(PDOException $e){
                    Logger::log("DATABASE", $e->getMessage());
                    return false;
            }           
        }
        public function updateRow($query, $params){
            return $this->insertRow($query, $params);
        }
        public function deleteRow($query, $params){
            return $this->insertRow($query, $params);
        }
    }