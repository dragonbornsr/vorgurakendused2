


<?php 
	require_once('config/config_db.php');
	require_once('dbConnectionPG.inc.php');
	require_once('connect.php');
	$database = new DataBase(DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME);
	//session_start();
	
?>

<!DOCTYPE html>
<html>
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>GIB DATA NAO</title>
  <meta name="description" content="Basic login page for ID-card authenticatuion and FB">
  <meta name="author" content="O.Tiit + A.-J. Toose">


  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href='//fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/custom.css">


</head>
<body>

<div id="wrapper" class="center"><h1>ID kaardi integreerimine Facebookiga</h1>

<?php 
	global $loginUrl;
	if (isset($_SESSION["isikukood"])) {
		echo "Isikukood: " . $_SESSION["isikukood"] . "<br>";
		echo "Eesnimi: " . $_SESSION["eesnimi"] . "<br>";
		echo "Perenimi: " . $_SESSION["perenimi"] . "<br>";
		if (isset($_SESSION["isikukood"])) {
			echo '<div id="fbbtn" class="four columns offset-by-six"><a href="' . $loginUrl . '" class = "button">Connect Facebook Account to ID card</a></div>';
		}
		echo "<div class='four columns offset-by-six'><a href='http://54.93.107.75/vorgurakendused2/logout.php' class='button button-primary'>Logi välja</a></div>";
	} else {
		echo "<div class='four columns offset-by-six'><a href='https://54.93.107.75/vorgurakendused2/register.php' class='button button-primary'>Logi Sisse ID Kaartiga või registreeri</a></div>";
		echo '<div id="fbbtn" class="four columns offset-by-six"><a href="' . $loginUrl . '" class = "button">Log in with Facebook</a></div>';
	}

?>
</div>
</body>
</html>
